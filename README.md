# Object Manipulation and Task Planning Course - Spring 2020

## Overview

This repository contains the exercises for the course OMTP completed by Group 861 of the second semester M.Sc. Robotics at Aalborg University.

## Getting Started

The repository is structured like so: A folder is made for each lecture when relevant. Each folder contains the assignments that have been completed by the group. In each folder there is also a `README.md` file for the individual lecture, here you will find an explanation of the exercises and it may contain additional prerequisites that are only relevant to that lecture.

### Prerequisites

`Ubuntu 18.04` and `ROS Melodic` is required. Lecture 3 and 4 require `Windows 10 64-bit` and `MATLAB R2019b`.

### Creating workspace and installing dependencies

```
# create workspace
mkdir -p omtp_course/src && cd omtp_course/src

# clone the repository
git clone --recursive https://your_username@bitbucket.org/lh7nlp/omtp.git

# install dependencies
sudo apt update -qq
rosdep update
rosdep install --from-paths . --ignore-src --rosdistro melodic -y

# install python catkin tools. Needed for catkin build command
sudo apt-get install python-catkin-tools

# build the workspace
cd ..
catkin build
```

## Authors

* **[Galadrielle Humblot-Renaux](mailto:ghumbl19@student.aau.dk)** - *Group member* - [Gala HR](https://bitbucket.org/%7B6419cd3a-c0e9-4955-8422-8c45662e040e%7D/)
* **[Guilherme Mateus Martins](mailto:gmateu16@student.aau.dk)**   - *Group member* - [Guilherme Mateus Martins](https://bitbucket.org/%7Bba72de4e-9cb6-4e73-89db-24d4d8f12fe7%7D/)
* **[Jan Kjær Jørgensen](mailto:jkja16@student.aau.dk)**           - *Group member* - [Jan Kjær Jørgensen](https://bitbucket.org/%7B342f1a45-adf0-43d0-856c-6a37d68d26d8%7D/)
* **[Jacob Krunderup Sørensen](mailto:jksa16@student.aau.dk)**     - *Group member* - [Jacob Krunderup Sørensen](https://bitbucket.org/%7Bc489c61d-40d9-449d-9964-13a350e237df%7D/)
* **[Lars Væhrens](mailto:lvahre16@student.aau.dk)**               - *Group member* - [Lars Væhrens](https://bitbucket.org/%7B084c4876-581c-4565-8d18-8fab9d54f81e%7D/)
* **[Rune Grønhøj](mailto:rgranh16@student.aau.dk)**               - *Group member* - [Rune Grønhøj](https://bitbucket.org/%7Be861c97c-c210-4770-bc27-9db291d95387%7D/)

## License

## Acknowledgements

* Aalborg University
* Dimi and Simon
