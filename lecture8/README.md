# Lecture 8

## Overview

In this lecture the the focus was on Convolutional Neural Networks (CNN) and Deep Learning (DL). A practical example was given and assignments were carried out in Google Colab.

## Getting Started

### Prerequisites

Any internet browser and a Google account with available space on Drive will work. However, for some exercises `Google Chrome` browser and a webcamera is required.

### Download required files

Some additional files are necessary to run the Google Colab environment and they can be downloaded from the [Course page on Moodle](https://www.moodle.aau.dk/course/view.php?id=32575&section=8), in the `Lecture material` folder download `covid19_colab.zip`, extract the zip-folder anywhere to your computer and upload it to your Google Drive. From here you want to open the `covid19_mask.ipynb` Notebook document. From here on out you follow the instructions given in the code files in Google Colab.


## Assignment 1

In this assignment the code was executed in Google Colab. All group members recorded a video of themselves without a facemask and then equipped one during the video to show that the detection of a facemask worked.

![Gala](gala.gif)![Gui](gui.gif)

![Jacob](jacob.gif)![Jan](jan.gif)

![Lars](lars.gif)![Rune](rune.gif)

*(The GIFs are at variable speeds)*

## Optional Assignment 1

**TODO: Exchange the MobileNetv2 with a network of our choice.**

## Optional Assignment 2

**TODO: Use the detection algorithm in the OMTP environment to train the simulated camera to identify two classes of any kind of objects.**
