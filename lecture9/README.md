# Lecture 9

## Overview

Deep reinforcement learning for robotics applications. A simple demo running DQN, PPO1, PPO2, and TRPO on the CartPole envrionment from OpenAI Gym.

## Getting Started

### Prerequisites

Make sure to have pip version >=19

```
pip install --upgrade pip
```

**Recommended:** Install and create a virtual python environment.

```
pip3 install virtualenv
```

In the `/lecture9/` folder create the virtual environment named `venv` using:

```
virtualenv venv
```

Before using the virtual environment source the workspace:

```
source ./venv/bin/activate
```

Now you are ready to install the dependencies, starting with Tensorflow<=1.15.0

```
pip3 install tensorflow==1.15.0
```

Install the separate module for GPU acceleration if training with a GPU is desired (CUDA also needs to be installed)

```
pip3 install tensorflow-gpu==1.15.0
```

Install OpenAI Gym

```
pip install gym
```

Install Stable Baselines

```
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev

pip install stable-baselines
```


### Additional dependencies that might be necessary

```
pip install --upgrade setuptools
pip install stable-baselines[mpi]
```


## Assignment 1

To train the model and run the animation of the inverted pendulum in the terminal window that has sourced the virtual environment run the following commmand:

```
python3 cart_pole_DQN.py
```

Where you can replace `DQN` with any of `PPO1`, `PPO2`, `TRPO` to train a different Deep Reinforcement Learning algorithm.

To view the graphs of the different models with tensorboard use the following command and open the website `http://localhost:6006/`

```
tensorboard --logdir logs
```

The models we trained can be seen in the plot below:

![Graph](Graph.png)

![Pole-cart](pole-cart-DQN.gif)
