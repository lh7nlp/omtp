%%
clc
clear all
close all
dt = 0.01;
path('.\apimex', path)
path('.\Mujoco_lib', path)
load test_trj.mat


%% The encoding step for the Joint DMP
[DMP, QDMP] = LearnQDMP(Qpath, dt);
JDMP = LearnJDMP(qJoints, dt);

%% Calculate final phase for the DMP integration
T_f = (length(Qpath) + 1) * DMP.dt;
Xmin = exp(-DMP.a_x * T_f / DMP.tau);

% Init. states for position DMP
Sp.y = DMP.y0;
Sp.z = zeros(1, 7);
Sp.x = 1;

% Init. states for joint position DMP
Sj.y = JDMP.y0;
Sj.z = zeros(1, 7);
Sj.x = 1;

% Init. states for quaternion DMP
Sq.q = QDMP.q0;
Sq.o = zeros(3, 1);
Sq.x = 1;

%% Execute in simulation
try
    % Close previous connection before starting simulation
    mj_close;
    % Connect to sim robot
    mj_connect;

    Startjoints = qJoints(1,:);

    %% Move to init. position
    JmoveM(Startjoints, 1);
    Con = mj_get_control;
    i = 1;
    tn = 0;
    st = tic;

    while Sp.x > Xmin
        %% DMP integration
        [Sp] = DMP_integrate(DMP, Sp, 0);

        %% Joint DMP integration
        [Sj] = DMP_integrate(JDMP, Sj, 0);
        jN(i,:)  = Sj.y;
        jjN(i,:) = Sj.z;

        %% Quaternion DMP integration
        [Sq] = qDMP_integrate(QDMP, Sq, 0);
        qQ(i,:) = double(Sq.q);   % just for plotting
        xN(i,:) = Sp.y;

        %% Send to the robot
        Con.ctrl(1:7)  = Sj.y;
        Con.ctrl(8:14) = Sj.z;
        mj_set_control(Con);

        %% Synchronisation
        tn = tn + dt;
        if tn > toc(st)
            pause(tn - toc(st))
        end
        i = i + 1;
    end
    toc

    %% Move to start position
    JmoveM(Startjoints, 1);

    % Close sim connection
    mj_close;

% Debugging
% Catch ME
%    ME % ErrorTrap(ME);
end
