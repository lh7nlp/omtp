%%
clc
clear all
close all
dt = 0.01;
load test_trj.mat

% Encode into DMP
[DMP, QDMP] = LearnQDMP(Qpath, dt);

% Function for calculating a Joint DMP parameters Inputs:
    % qJoints and dt Output: JDMP parameters
JDMP = LearnJDMP(qJoints, dt);

%% Calculate final phase for the DMP integration
T_f = (length(Qpath) + 1) * DMP.dt;
Xmin = exp(-DMP.a_x * T_f / DMP.tau);

% Init. states for position dmp
Sp.y = DMP.y0;
Sp.z = zeros(1, 7);
Sp.x = 1;

% Init. states for position dmp
Sj.y = JDMP.y0;
Sj.z = zeros(1, 7);
Sj.x = 1;

% Init. states for quaternion dmp
Sq.q = QDMP.q0;
Sq.o = zeros(3, 1);
Sq.x = 1;

%%
i = 1;
while Sp.x > Xmin
    % Position DMP integration
    [Sp] = DMP_integrate(DMP, Sp, 0);
    xN(i,:) = Sp.y;

    %%  Joint DMP integration
    [Sj] = DMP_integrate(JDMP,Sj,0);
    jN(i,:) = Sj.y;

    % Quaternion DMP integration
    [Sq] = qDMP_integrate(QDMP, Sq, 0);
    qQ(i,:) = double(Sq.q);   % just for plotting

    i = i + 1;
end

%% Plot Position part of the trajectory
plot(xN, 'r')   % Plot DMP trajectory
hold on
title('Position part of the trajectory')
xlabel('Points of the trajectory')
plot(Qpath(:,1:3), '--b')   % Plot example trajectory

% Plot Orientation part of the trajectory
figure(2)
plot(qQ, 'r')   % Plot QDMP trajectory
hold on
title('Orientation part of the trajectory')
xlabel('Points of the trajectory')
plot(Qpath(:,4:7), '--b')   % Plot example trajectory

% Plot Joint trajectories
figure(3)
plot(jN, 'r')   % Plot QDMP trajectory
hold on
title('Joint part of the trajectory')
xlabel('Points of the trajectory')
plot(qJoints(:,1:3), '--b')   % Plot example trajectory
