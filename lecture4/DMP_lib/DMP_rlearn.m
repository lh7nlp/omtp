function [DMP] = DMP_rlearn(y, DMP)

% Locally waighetd regression DMP,

% Input measured values
%   y matrix of signals
%   y,   ... position(t)
%   dy,  ... velocity(t)
%   ddy, ... acceleration(t)
%   dt,  ... sample time
%   DMP, ... DMP parameters

% DMP parameters
%   N,      ... number of Gaussian kernel functions
%   w,      ... weight vector of size(Nx1)
%   c,      ... center
%   sigma2, ... variance
%   tau,    ... time scaling
%   a_x,    ...
%   a_z,    ... positive gains

%% Params - global
[NT, NS] = size(y);

% Initial state of the DMP
DMP.y0 = y(1,:);        % initial value of the demonstrated trajectory

% The goal state of the DMP
DMP.goal  = y(NT,:);    % the last point of the demonstrated trajecory

% Length of the recorded trajectory
DMP.tau = (NT - 1) * DMP.dt;

%% Derivatives for the entire trajecory
% Velocities
dy = diff(y) / DMP.dt;
dy = [zeros(1, NS); dy];
% Acceleration
ddy = diff(dy) / DMP.dt;
ddy = [zeros(1, NS); ddy];

DMP.dy0 = dy(1,:);    % initial value

%% Init. params for target traj. and fitting
x = 1;
h = -0.5;
dx = 0;
for i = 1:NS,
     P(:,:,i) = eye(DMP.N) * 1000;    % initial large covariance
end
DMP.w = zeros(DMP.N, NS);             % initial weights

%% Definition of the gausian kernel functions
c_lin = linspace(0, 1, DMP.N);
DMP.c = exp(-DMP.a_x * c_lin);
DMP.sigma2 = (diff(DMP.c) * 0.75).^2;
DMP.sigma2 = [DMP.sigma2, DMP.sigma2(end)];

cutoff = 0.001;
lambda = 0.995;

%% Loop that fits all points of the trajectory
for t = 1:NT,
    %% The weighted sum of the locally weighted regression models
    psi = exp(-0.5 * (x - DMP.c).^2 ./ DMP.sigma2)';
    % Derivatives
    dx = -DMP.a_x * x;

    %% The temporal scaling derivatives
    dx = dx / DMP.tau;
    % Euler integration
    x = x + dx * DMP.dt;

    for k = 1:NS
        %% Target for fitting, the second order DMP equations at this point
        ft = (DMP.tau^2 * ddy(t, k) - DMP.a_z * (DMP.a_z / 4 * (DMP.goal(k) - y(t, k)) - DMP.tau * dy(t, k)));

        %% Recursive regression step
        xx = psi * x / sum(psi);
        % Calculation of all weights over entire interval x
        Pk = P(:,:,k);
        P(:,:,k) = (Pk-(Pk*xx*xx'*Pk)/(1+xx'*Pk*xx));
        e = ft-xx'*DMP.w(:,k);
        DMP.w(:,k) = DMP.w(:,k)+e*P(:,:,k)*xx;
    end
end
