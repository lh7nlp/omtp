function [JDMP] = LearnJDMP(path, dt, N)
    % Set DMP parameters
    if nargin == 3
        DMP.N  = N;
        JDMP.N = N;
    else
        DMP.N  = 100;
        JDMP.N = 100;
    end
    DMP.dt  = dt; DMP.a_z  = 48;     DMP.a_x  = 2;
    JDMP.dt = dt; JDMP.a_z = 48 * 3; JDMP.a_x = 2;
    % Learning joint positions
    ppath = path(:, 1:3);
    JDMP = DMP_rlearn(ppath, DMP);
end
