%% DMP move and grasp of cubes
clc
clear all
close all
dt=0.01;
path('.\apimex',path)
path('.\Mujoco_lib',path)

% Close previous connection before starting simulation
mj_close;
% Connect to sim robot
mj_connect;

%% Robot positions
q0 = [0       0      0 0       0 0      0]; % arm starting position
q1 = [0       0      0 -pi/2   0 pi/2   0]; % above cubes position
q3 = [-1.5708 0.8960 0 -1.4466 0 0.8481 0]; % deposit cube position

%% Cube positions
C(1,:) = [-0.4, 0,    0.02];
C(2,:) = [-0.4, -0.4, 0.02];
C(3,:) = [-0.5, 0.3,  0.02];
C(4,:) = [-0.5, -0.3, 0.02];
C(5,:) = [-0.4, 0.4,  0.02];
C(6,:) = [-0.6, 0,    0.02];

for i = 1:length(C)
    
    JmoveDynamic(q1, 2, dt);

    Gripper('open')
    CmoveDynamic([0, -1, 0; -1, 0, 0; 0, 0, -1],C(i, :),4,[0 0 0.14],dt);
    Gripper('close')
    pause(2)

    JmoveDynamic(q1, 4, dt);
    JmoveDynamic(q3, 4, dt);
    Gripper('open')

end
JmoveDynamic(q1, 2, dt);
mj_close

%-------------------------------------------------------------------------
function JointMoveDynamic = JmoveDynamic(goal, t, dt)
%-------------------------------------------------------------------------
% inputs:
    % goal is a vector of joint positions
    % t    is the duration of the movement
    % dt   is the sample time

    Con = mj_get_control; 
    i = 1;
    tn = 0;
    tic;
    
    %% Parameters from LearnJDMP.m
    JDMP.N = 2;
    JDMP.dt = dt; 
    JDMP.a_z = 48; 
    JDMP.a_x = 2;
    
    %% Parameters from DMP_rlearn.m
    % Params - global
    JDMP.y0 = Con.ctrl(1:7);        % initial position of robot
    JDMP.goal = goal;
    JDMP.tau = 2 * t;               % length of trajectory (time scaling)
    % Derivatives for the entire trajecory
    JDMP.dy0 = [0 0 0 0 0 0 0];     % velocity
    % Init. params for target traj. and fitting
    JDMP.w = [0 0 0 0 0 0 0;        % weights
              0 0 0 0 0 0 0];
    % Definition of the gausian kernel functions
    JDMP.c = [1 0.1353];            % center
    JDMP.sigma2 = [0.4206 0.4206];  % variance

    %% Script from MoveDMP_Mujoco.m
    % Calculate final phase for the DMP integration
    T_f = t;
    Xmin = exp(-JDMP.a_x * T_f / JDMP.tau);

    % Init. states for joint position DMP
    Sj.y = JDMP.y0;
    Sj.z = zeros(1,7);
    Sj.x = 1;
    
    % Execute in simulation
    while Sj.x > Xmin
        % Joint DMP integration
        [Sj] = DMP_integrate(JDMP, Sj, 0);

        % Send to the robot
        Con.ctrl(1:7)  = Sj.y;
        Con.ctrl(8:14) = Sj.z;
        mj_set_control(Con);
        
        % Synchronisation
        tn = tn + dt;
        if tn > toc
            pause(tn - toc)
        end
        i = i + 1;
    end
    toc
end

%-------------------------------------------------------------------------
function CartesianMoveDynamic = CmoveDynamic(RotGoal, PosGoal, t, tool, dt)
%-------------------------------------------------------------------------
% inputs:
    % RotGoal is a rotation matrix
    % PosGOal is the goal position to move to
    % t       is the duration of the movement
    % tool    is the row vector (3x1) of the tool centre point
    % dt      is the sample time

    Con = mj_get_control; 
    i = 1;
    tn = 0;
    tic
    j0 = Con.ctrl(1:7);                 % 7x1 initial joint positions
    % kinematic derivation of lwr       % p0 3x1 task position
    [p0, R0] = kinjac_lwr(j0, tool);    % R0 3x3 rotational matrix
    
    %% Parameters from LearnQDMP.m
    QDMP.N = 2;
    QDMP.dt = dt; 
    QDMP.a_z = 48; 
    QDMP.a_x = 2;
    
    %% Parameters from qDMP_learn.m
    % Params - global
    QDMP.q0 = quaternion(R0);       % initial position of robot covnerted
                                    % from rotation matrix to quaternion
    QDMP.qg = quaternion(RotGoal);  % goal position of robot converted
                                    % from rotation matrix to quaternion
    QDMP.goal = PosGoal;            % discrete move DMP goal position
                                    % in world coordinates
    QDMP.tau = 2 * t;               % length of trajectory (time scaling)
    % Derivatives for the entire trajecory
    QDMP.dy0 = [0 0 0];             % velocity
    % Init. params for target traj. and fitting
    QDMP.w = [0 0 0; 0 0 0];        % weights
    % Definition of the gausian kernel functions
    QDMP.c = [1 0.1353];            % center
    QDMP.diag = [1 1 1];            % diagnoal for angular velocity
    QDMP.sigma2 = [0.4206 0.4206];  % variance

    %% Script from MoveDMP_Mujoco.m
    % Calculate final phase for the DMP integration
    T_f = t;
    Xmin = exp(-QDMP.a_x * T_f / QDMP.tau);

    % Init. states for position DMP (discrete move)
    Sp.y = p0;
    Sp.z = zeros(1,3);
    Sp.x = 1;
    
    % Init. states for quaternion DMP
    Sq.q = QDMP.q0;
    Sq.o = zeros(3,1);
    Sq.x = 1;
    
    % prev_pos of robot initially stored as the one read from MuJoCo
    prev_pos = j0;
    
    % Execute in simulation
    while Sq.x > Xmin
        % Regular DMP integration (discrete move)
        [Sp] = DMP_integrate(QDMP, Sp, 0);
        % Quaternion DMP integration
        [Sq] = qDMP_integrate(QDMP, Sq, 0);
        % Calculate inverse kinematics of the robot to get joint positions
        % given a 3x1 position vector, 3x3 rotational matrix,
        % tool center position transposted, previous position of the robot
        joint_pos = ikin_lwr(Sp.y(1:3),quat2rotm(double(Sq.q)),tool',prev_pos(:));
        
        % Send to the robot
        Con.ctrl(1:7)  = joint_pos;
        mj_set_control(Con);
        
        % Set current position to previous position
        %prev_pos = joint_pos;
        
        % Synchronisation
        tn = tn + dt;
        if tn > toc
            pause(tn - toc)
        end
        i = i + 1;
    end
    toc
end
