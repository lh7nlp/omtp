%%
clc
clear all
close all
dt=0.01;
path('.\apimex',path)
path('.\Mujoco_lib',path)

% Close previous connection before starting simulation
mj_close;
% Connect to sim robot
mj_connect;

%% Robot positions
q0 = [0       0      0 0       0 0      0]; % arm starting position
q1 = [0       0      0 -pi/2   0 pi/2   0]; % above cubes position
q3 = [-1.5708 0.8960 0 -1.4466 0 0.8481 0]; % deposit cube position

%% Cube positions
C(1,:) = [-0.4, 0,    0.02];
C(2,:) = [-0.4, -0.4, 0.02];
C(3,:) = [-0.5, 0.3,  0.02];
C(4,:) = [-0.5, -0.3, 0.02];
C(5,:) = [-0.4, 0.4,  0.02];
C(6,:) = [-0.6, 0,    0.02];

for i = 1:length(C)

    JmoveM(q1, 2);

    Gripper('open')
    T = MakeT([0, -1, 0; -1, 0, 0; 0, 0, -1] ,C(i, :));
    CmoveM(T, 4, [0 0 0.14]);
    Gripper('close')
    pause(2)

    JmoveM(q1, 4);
    JmoveM(q3, 4);
    Gripper('open')


end
JmoveM(q1, 2);
mj_close
