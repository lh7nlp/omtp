# Lecture 4

## Overview

In this lecture the focus has been on implementing Dynamic Motion Primitives (DMP) in MATLAB and in the simulation environment MuJoCo. Some template files for MATLAB were provided together with the KUKA LWR manipulator for MuJoCo HAPTIX. In these template files the group has filled in the missing portions to make the program run as described by the objective for the exercise.

## Getting Started

### Prerequisites

`Windows 10 64-bit` and `MATLAB R2019b` or newer version is required to use MuJoCo HAPTIX.

From the [MuJoCo download page](https://www.roboti.us/index.html) you will have to download the `mjhaptix150` and install it.

### Download required files

Some additional files are necessary to run the simulation in MuJoCo and they can be downloaded from the [Course page on Moodle](https://www.moodle.aau.dk/course/view.php?id=32575&section=4), download `Exercises updated.zip`, extract the zip-folder in the directory you wish to run the simulation from. Now you are ready to clone the `lecture4\DMP_lib` folder into the previously made directory so that the files from this repository replaces the MATLAB script in the zip-folder.

In MATLAB you want to open the folder `DMP_lib` so that you can run the scripts, in addition the following folders: `apimex` and `Mujoco_lib` should be added to the path so that MATLAB can find the necessary functions to run the simulation.

## Exercise 1

In this exercise a DMP has been implemented in MATLAB. To run the DMP and see how it can follow a demonstrated trajectory(`test_trj.mat`) open the `DMP_lib\Test_CDMP.m` file and run the script.

The figures below shows how the DMP is able to follow the trajectory.

![Figure1](fig1.png)![Figure2](fig2.png)![Figure3](fig3.png)


## Exercise 2

In this exercise a DMP has been implemented in the `DMP_lib\MoveDMP_Mujoco.m` file, similar to the previous exercise the robot will follow a demonstrated trajectory(`test_trj.mat`). To run the simulation open the directory `.\program` and launch the `mjhaptix.exe` program. This will open MuJoCo HAPTIX and you will now have to load the simulation model into the program to run it. To do so, click on file menu-button(`ctrl+o`) and find the `LWR_table.xml` file in the `.\Haptix_models` directory.

You are now ready to start the simulation by pressing the play-button(`space-bar`). The simulation is now running and will be waiting for any MATLAB control input. Go back to `DMP_lib\MoveDMP_Mujoco.m` file and run the script so that the KUKA LWR manipulator will execute the trajectory.

![Exercise2](exercise2.gif)

## Exercise 3

In this exercise the DMP similar to the one used in the previous exercise was adapted for the task of grasping cubes on the table. The objective was to use the DMP to plan a linear path between point-to-point movements to pick up the cubes and places them.

A `Test_move_grasp.m` script was provided showing how the robot picks and places the cubes using simple kinematics. The same approach was taken in `Test_dynamic_move_grasp.m`, however, two new functions were created to make use of DMP for joint moovement and cartesian movement. The same joint positions and world coordinates were used to give a target for where the robot should move. The `LearnJDMP.m`, `LearnQDMP.m`, `DMP_rlearn.m`, and `qDMP_learn.m` scripts were used as inspiration to create the functions `JmoveDynamic()` and `CmoveDynamic()` that would replace `JmoveM()` and `CmoveM()` respectively. To connect to the robot and move the joints the approach used in `MoveDMP_Mujoco.m` was followed. A comparison of the two different scripts can be seen below:

![Exercise3](exercise3.gif)

*(The above GIF is at 2x speed)*

**TODO: Fix the funny behaviour of the arm and make sure it grasps correctly**

## Exercise 4

**TODO: Use the functionality of the previous exercise, but built a function that measures contact force of the manipulator, so that it detects collisions.**
