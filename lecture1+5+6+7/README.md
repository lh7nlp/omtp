# Lecture 1, 5, 6, and 7.

## Overview

### Lecture 1

First and foremost some tutorials on URDF and XACRO was done. A simulation of a factory was built and modified. In the exercise some of the bins, robot, and pedestals were moved to a different location. A new ABB IRB 6640 manipulator from the ROS-industrial was added to the simulation. Additional objects were made in XACRO and added to the factory scene.

### Lecture 5

The subject of this lecture was an introduction to MoveIt. In this lecture the first objective was to build the MoveIt package for the simulated factory using **MoveIt Setup Assistant**. The second objective was to write and run a MoveIt Commander script to make the manipulators in the factory move from point to point. For the third assignment the robots are programmed to execute a simple pick and place behavior.    

### Lecture 6

In this lecture the topic is focused on object detection and grasping. In the Gazebo simulation two logical cameras are introduced and vacuum grippers are equipped on the two robots. The combination of the two will make it possible to manipulate objects in a dynamic environment by estimating the post of the object of interest and manipulating it.

### Lecture 7

In this lecture **FlexBE**, the flexible behavior engine, is used to create a finite state machine (FSM) for the manipulation of objects on the conveyor belt in the OMTP factory. The assignment demonstrates a pick and place behavior where the conveyor moves the object to the camera position and the FSM will move the manipulator in position to grasp the object and move it.

## Getting Started

### Prerequisites

`Ubuntu 18.04` and `ROS Melodic` is required.

### Installing dependencies

<!-- No additional dependencies other than those mentioned in the `README.md` file found in the root of this repository is necessary. -->

Install ABB industrial dependency: 

```
sudo apt install ros-melodic-abb
```

Install the MoveIt setup assistant:

```
sudo apt install ros-melodic-moveit-setup-assistant
```

Install FlexBe:

```
# install behaviour engine as binary
sudo apt install ros-$ROS_DISTRO-flexbe-behavior-engine

# install the graphical app
git clone https://github.com/FlexBE/flexbe_app.git

catkin build

# create shortcut
rosrun flexbe_app shortcut create # or "remove" to remove it again
```

### The Assignments

All of the assignments are found in individual folders. The description of how to run each of the assignments is described in detail in a separate README file in each folder.
