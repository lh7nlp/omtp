#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from omtp_factory_flexbe_states.set_conveyor_power_state import SetConveyorPowerState
from omtp_factory_flexbe_states.detect_part_camera_state import DetectPartCameraState
from omtp_factory_flexbe_states.compute_grasp_state import ComputeGraspState
from flexbe_manipulation_states.moveit_to_joints_dyn_state import MoveitToJointsDynState as flexbe_manipulation_states__MoveitToJointsDynState
from omtp_factory_flexbe_states.vacuum_gripper_control_state import VacuumGripperControlState
from flexbe_manipulation_states.srdf_state_to_moveit import SrdfStateToMoveit as flexbe_manipulation_states__SrdfStateToMoveit
from omtp_factory_flexbe_states.control_feeder_state import ControlFeederState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu May 07 2020
@author: Gr 861
'''
class Robot1pickupfromconveyorSM(Behavior):
	'''
	A box is added to the conveyor, for robot1 to pick up, from the conveyor using moveit and a logical camera to detect the object.
	'''


	def __init__(self):
		super(Robot1pickupfromconveyorSM, self).__init__()
		self.name = 'Robot1 pick up from conveyor '

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		r1_group = 'robot1'
		pi = 3.1415
		r1_joint_names = ['robot1_shoulder_pan_joint', 'robot1_shoulder_lift_joint', 'robot1_elbow_joint', 'robot1_wrist_1_joint', 'robot1_wrist_2_joint', 'robot1_wrist_3_joint']
		# x:803 y:766, x:26 y:666
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.Pose_obj = []
		_state_machine.userdata.r1_joint_names = []
		_state_machine.userdata.r1_joint_values = []
		_state_machine.userdata.Speed = 100

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:30 y:24
			OperatableStateMachine.add('Activate conveyor',
										SetConveyorPowerState(stop=False),
										transitions={'succeeded': 'Activate feeder', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'Speed'})

			# x:205 y:230
			OperatableStateMachine.add('Detect object on conveyor',
										DetectPartCameraState(ref_frame='world', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'Move robot1 to preGrasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'Pose_obj'})

			# x:323 y:363
			OperatableStateMachine.add('Compute robot1 preGrasp',
										ComputeGraspState(group=r1_group, offset=0.3, joint_names=r1_joint_names, tool_link='vacuum_gripper1_suction_cup', rotation=pi),
										transitions={'continue': 'Move robot1 to preGrasp 2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'Pose_obj', 'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:387 y:432
			OperatableStateMachine.add('Move robot1 to preGrasp 2',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_group, action_topic='/move_group'),
										transitions={'reached': 'Compute robot1 grasp', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:427 y:499
			OperatableStateMachine.add('Compute robot1 grasp',
										ComputeGraspState(group=r1_group, offset=0.16, joint_names=r1_joint_names, tool_link='vacuum_gripper1_suction_cup', rotation=pi),
										transitions={'continue': 'Move robot1 to Grasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'Pose_obj', 'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:469 y:572
			OperatableStateMachine.add('Move robot1 to Grasp',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_group, action_topic='/move_group'),
										transitions={'reached': 'Activate Gripper', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:534 y:648
			OperatableStateMachine.add('Activate Gripper',
										VacuumGripperControlState(enable='true', service_name='/gripper1/control'),
										transitions={'continue': 'Move robot1 to home 2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:598 y:723
			OperatableStateMachine.add('Move robot1 to home 2',
										flexbe_manipulation_states__SrdfStateToMoveit(config_name='R1Home', move_group=r1_group, action_topic='/move_group', robot_name=""),
										transitions={'reached': 'finished', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:259 y:296
			OperatableStateMachine.add('Move robot1 to preGrasp',
										flexbe_manipulation_states__SrdfStateToMoveit(config_name='R1PreGrasp', move_group=r1_group, action_topic='/move_group', robot_name=""),
										transitions={'reached': 'Compute robot1 preGrasp', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:156 y:163
			OperatableStateMachine.add('Move robot1 to home',
										flexbe_manipulation_states__SrdfStateToMoveit(config_name='R1Home', move_group=r1_group, action_topic='/move_group', robot_name=""),
										transitions={'reached': 'Detect object on conveyor', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'r1_joint_values', 'joint_names': 'r1_joint_names'})

			# x:91 y:95
			OperatableStateMachine.add('Activate feeder',
										ControlFeederState(activation='true'),
										transitions={'succeeded': 'Move robot1 to home', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
