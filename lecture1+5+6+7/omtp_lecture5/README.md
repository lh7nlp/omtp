# Lecture 5 Configuring MoveIt

Source the workspace:

```
source devel/setup.bash
```

Launch:

```
roslaunch moveit_setup_assistant setup_assistant.launch
```

Generate a MoveIt configuration as follows:

- **Start** > Load the `omtp_factory.xacro` from the `omtp_support` package.
- **Self-Collisions** > Set sampling density to high and generate collision matrix
- **Virtual Joints** > Create a virtual joint `fixed_base1` with `world` as child link and `robot1_base_link` as parent
- **Virtual Joints** > Do the same thing for `robot2`
- **Planning Groups** > Create new planning group called `robot1` with *TRAC-IK Kinematics Solver* and *RRTConnect* planner
- **Planning Groups** > Add a kinematic chain with `robot1_base_link` as base and `vacuum_gripper1_suction_cup` as link
- **Planning Groups** > Do the same for `robot2`
- **Robot Poses** > Add Up and Home poses for both robots
- **ROS Control** > Use the *Auto Add FollowJointsTrajectory* Controller
- **Simulation** > Generate URDF
- **Configuration Files** > Set the Configuration path to ``omtp_course_ws/src/omtp/omtp_moveit_config``

## Using MoveIt

Build and source the workspace first:

```
catkin build
source devel/setup.bash
```

### RViz

Launch the demo in Rviz:

```
roslaunch omtp_moveit_config demo.launch
```

### MoveIt Commander

Launch MoveIt commander:

```
rosrun moveit_commander moveit_commander_cmdline.py
```

Test the commander with the following commands:

```
use robot1
go R1Up
go R1Home
go up 0.1
go down 0.3
help
```

To load commands from a script (eg. moveit_commander_test), use:

```
load moveit_commander_test
```

### Move Group Python Interface

Make scripts executable:

```
chmod +x omtp_lecture5/scripts/*
```

Test the pick and place pipeline:

```
roslaunch omtp_lecture5 omtp_simple_pick_place.launch
```

Test assignment 3 script:
```
roslaunch omtp_lecture5 lecture5_assignment3.launch
```

### Gazebo


Test our pick and place pipeline:

```
roslaunch omtp_lecture5 omtp_lecture5_environment.launch
roslaunch omtp_lecture5 omtp_lecture5_environment.launch gui:=false # no GUI
```

Note that Gazebo is launched in pause state to make sure that all controllers have been loaded. You can either press play in the GUI or from the terminal as follows:

```
rosservice call /gazebo/unpause_physics "{}"
```

## Final result

![Demo](assignment3.gif)
