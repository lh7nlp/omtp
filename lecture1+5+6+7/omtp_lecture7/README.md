# Lecture 7

## Assignment 1

In the **FlexBE App** a finite state machine has been created to use the camera to detect the object on the conveyor belt. When the pose of the object is retrieved a pre-grasp pose of the robot is calculated and the manipulator is moved right above the object. Next, the grasp pose is calculated and the robot is moved to grasp the object by activating the vacuum gripper. Then the object is moved by the manipulator to the home position.

Source the workspace:

```
source devel/setup.bash
```

Launch:

```
roslaunch omtp_gazebo omtp_pick_demo.launch
roslaunch flexbe_app flexbe_full.launch
```

Spawn the object on conveyor:

```
rosservice call /start_spawn
```
Note: the position of the spawned object was modified in `omtp_gazebo/config/conveyor_object.yaml` such that the object initially appears outside of the camera's view, and is then brought to the robot using the conveyor belt.

In the **FlexBE App** window load the `Robot1 pick up from conveyor` state machine from the `omtp_factory_flexbe_behaviors` package and execute the behavior in the Runtime Control window.

The FSM made in **FlexBE**:

![FlexBE](assignment1.png)

Video of the movement executed in **Gazebo**:

![object to gripper](assignment1.gif)

*(The above GIF is at 2x speed)*
