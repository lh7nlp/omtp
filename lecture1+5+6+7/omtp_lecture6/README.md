# Lecture 6


## Assignment 1

The logical camera 1 and 2 have been added to the simulation and can be viewed using the following:

Source the workspace:

```
source devel/setup.bash
```

Launch:

```
roslaunch omtp_lecture6 lecture6_assignment1.launch
```

## Assignment 2

The factory simulation is launched from the previous exercise and in a separate terminal `rosrun tf view_frames` is executed to view the transformation tree of all the objects within the simulation.

The tf tree can be viewed in the image below:

![tf tree](tf_tree.png)

## Assignment 3

Source the workspace and then launch:

```
roslaunch omtp_lecture6 lecture6_assignment3.launch
```

To spawn the object next to robot 2 use the following ROS-service:

```
rosservice call /spawn_object_at_robot2
```

A box object is added to the factory floor and the `lecture6_assignment3.py` script is edited to implement a subscriber that listens to the `logical_camera_2` topic, here the relevant reference frame for the pose of the object can be obtained.

The frame of the object is then transform to the `vacuum_gripper2_suction_cup` reference frame so that the movement from pre-grasp position and to the object can be calculated.

## Assignment 4

In this assignment a `lecture6_assignment4.py` script was written to move to the location of the object, the gripper is activated to suck the object, move to the  location above the bin and release the object by disabling the gripper.

![object to bin](assignment4.gif)


## Assignment Challenge

**TODO:**
