#!/usr/bin/env python

import rospy
import sys
from math import pi
import numpy

import actionlib
import moveit_commander
import tf2_ros
import geometry_msgs
import tf2_geometry_msgs
import moveit_msgs
from omtp_gazebo.msg import LogicalCameraImage
from omtp_gazebo.srv import VacuumGripperControl
from tf.transformations import quaternion_from_euler

global robot2_group
global robot2_client

def logical_camera2(data):
    # Get object pose
    if (data.models[-1].type == 'object'):
        # Create a pose stamped message type from the camera image topic.
        object_pose = geometry_msgs.msg.PoseStamped()
        object_pose.header.stamp = rospy.Time.now()
        object_pose.header.frame_id = "logical_camera_2_frame"
        object_pose.pose = data.models[-1].pose
        while True:
            try:
                object_world_pose = tf_buffer.transform(object_pose, "world")
                return object_world_pose
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                print(e)
                continue
    else:
        # Do nothing.
        print('')


def gripper(succ): # gripper-sucks object, very useful comment
    '''Grasp with the /gripper2/control service if True the succing is enabled'''
    gripper_client = rospy.ServiceProxy('/gripper2/control', VacuumGripperControl)
    gripper_client.call(succ)


def move_to_named_target(target):
    '''Move robot2 to a named target defined in moveit setup assistant'''
    global robot2_group, robot2_client
    robot2_group.set_named_target(target)
    robot2_group.go()


def move_to_target(pose):
    '''Move robot 2 to pose target'''
    global robot2_group, robot2_client
    robot2_group.set_pose_target(pose)
    robot2_group.go()


if __name__ == '__main__':
    global robot2_group, robot2_client
    # Initialize ROS node and moveit_commander.
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('lecture6_assignment4', anonymous=True)

    robot2_group = moveit_commander.MoveGroupCommander("robot2")

    robot2_client = actionlib.SimpleActionClient('execute_trajectory',
                                                 moveit_msgs.msg.ExecuteTrajectoryAction)
    robot2_client.wait_for_server()
    rospy.loginfo('Execute Trajectory server is available for robot2')

    # Create a TF buffer in the global scope
    tf_buffer = tf2_ros.Buffer()
    tf_listener = tf2_ros.TransformListener(tf_buffer)

    move_to_named_target("R2PreGrasp")
    # Subscribe to the logical camera topic.
    message = rospy.wait_for_message('/omtp/logical_camera_2', LogicalCameraImage)
    pose = logical_camera2(message)

    print "Going to object pose"
    rotation = quaternion_from_euler(pi, 0, pi/2)
    pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w = rotation
    pose.pose.position.z += -0.04
    move_to_target(pose)
    print "Succing the object"
    gripper(True)
    print "Move object up"
    pose.pose.position.z += 0.2
    move_to_target(pose)
    print "Move to bin"
    move_to_named_target("R2PreGrasp")
    gripper(False)
