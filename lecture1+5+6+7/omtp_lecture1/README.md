# Lecture 1 Running the RViz simulation

Go to the directory of the cloned repository:

```
cd omtp
```

Source the workspace:

```
source devel/setup.bash
```

Launch the launch file for the given exercise, e.g.:

```
roslaunch omtp_support visualize_omtp_factory.launch
```

**TODO: Perhaps add some explanations of the individual exercises and how they were done?**
