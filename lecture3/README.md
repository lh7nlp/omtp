# Lecture 3

## Overview

This lecture did not contain any exercises, however, a Simulink control system was made for visualizing and learning how a Dynamic Motion Primitives functions, build in the same way as shown in the video lectures.

## Getting Started

### Prerequisites

`MATLAB R2019b` with `Simulink (Version 10.0)` or newer version is required.

### Running the Simulink simulation

First run the `init_force.m` MATLAB script to load the parameters into the workspace. In this script the gains `alpha_z` and `beta_z` can be modified accordingly to change the dampening of the system.

Secondly, run the `DMP.slx` Simulink file and click on any of the three scopes to show the plots for position, velocity and acceleration. Some parameters can be adjusted using the sliders or direct input that is: `Goal`, `tau`, `Fext`, and `Omega`.
