%% Parameters for the Simulink control system
% Run script to load parameters into workspace before using the Simulink simulation

% Initial value
x_0 = 0;

% Wall
x_wall = 0.5;
K_wall = 250;

% DMP
Goal  = 1;
alpha = 48;
beta  = alpha/4;
tau   = 1;
Fext  = 10;

% Gains
alpha_z = 12;
beta_z  = alpha_z / 4;