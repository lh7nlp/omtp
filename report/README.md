# Object Manipulation and Task Planning Course - Spring 2020

## Table of Contents

||Page|
|---|:---:|
|[Authors](README.md#authors)|1|
|[Overview](README.md#overview)|1|
|[Getting Started](README.md#getting-started)|2|
|[Lecture 1 Running the RViz Simulation](README.md#lecture-1-running-the-rviz-simulation)|3|
|[Lecture 2 Git and Version Control](README.md#lecture-2-git-and-version-control)|3|
|[Lecture 3 Simulink Dynamic Motion Primitives Simulation](README.md#lecture-3-simulink-dynamic-motion-primitives-simulation)|4|
|[Lecture 4 Dynamic Motion Primitives in MuJoCo](README.md#lecture-4-dynamic-motion-primitives-in-mujoco)|5|
|[Lecture 5 Configuring MoveIt and using it in Gazebo](README.md#lecture-5-configuring-moveit-and-using-it-in-gazebo)|7|
|[Lecture 6 Adding Logical Cameras to Gazebo](README.md#lecture-6-adding-logical-cameras-to-gazebo)|10|
|[Lecture 7 Implementing a FlexBE Finite State Machine in Gazebo](README.md#lecture-7-implementing-a-flexbe-finite-state-machine-in-gazebo)|12|
|[Lecture 8 Implementing Convolutional Neural Networks and Deep Learning in Google Colab](README.md#lecture-8-implementing-convolutional-neural-network-and-deep-learning-in-google-colab)|14|
|[Lecture 9 Implementing Deep Reinforcement Learning with OpenAI Gym](README.md#lecture-9-implementing-deep-reinforcement-learning-with-openai-gym)|15|
|[Acknowledgements](README.md#acknowledgements)|16|

## Authors

* **[Gala Humblot-Renaux](mailto:ghumbl19@student.aau.dk)**        - *Group member*
* **[Guilherme Mateus Martins](mailto:gmateu16@student.aau.dk)**   - *Group member*
* **[Jan Kjær Jørgensen](mailto:jkja16@student.aau.dk)**           - *Group member*
* **[Jacob Krunderup Sørensen](mailto:jksa16@student.aau.dk)**     - *Group member*
* **[Lars Væhrens](mailto:lvahre16@student.aau.dk)**               - *Group member*
* **[Rune Grønhøj](mailto:rgranh16@student.aau.dk)**               - *Group member*

## Overview

This report contains the assignments for the OMTP course completed by Group 861 at the second semester M.Sc. Robotics at Aalborg University.

The repository is available at: https://bitbucket.org/lh7nlp/omtp/src/master/ (To view the GIFs use see the READMEs on the website)

<div style="page-break-after: always;"></div>

## Getting Started

The repository is structured like so: A folder is made for each lecture when relevant. Each folder contains the assignments that have been completed by the group. In each folder there is also a `README.md` file for the individual lecture, here you will find an explanation of the assignments and it may contain additional prerequisites that are only relevant to that lecture.

### Prerequisites

`Ubuntu 18.04` and `ROS Melodic` is required. Lecture 3 and 4 require `Windows 10 64-bit` and `MATLAB R2019b`.

### Creating workspace and installing dependencies

```shell
# create workspace
mkdir -p omtp_course/src && cd omtp_course/src

# clone the repository
git clone --recursive https://your_username@bitbucket.org/lh7nlp/omtp.git

# install dependencies
sudo apt update -qq
rosdep update
rosdep install --from-paths . --ignore-src --rosdistro melodic -y

# install python catkin tools. Needed for catkin build command
sudo apt-get install python-catkin-tools
sudo apt install ros-melodic-abb

# build the workspace
cd ..
catkin build
```

<div style="page-break-after: always;"></div>

## Lecture 1 - Running RViz Simulation

### Overview

First and foremost some tutorials on URDF and XACRO was done. A simulation of a factory was built and modified. In the exercise some of the bins, robot, and pedestals were moved to a different location. A new ABB IRB 6640 manipulator from the ROS-industrial was added to the simulation. Additional objects were made in XACRO and added to the factory scene.

### Assignment 1

The first and only assignment focused on the spring clean up of the factory environment. The changes can be viewed by using the following directions:

Go to the directory of the cloned repository:

```shell
cd omtp
```

Source the workspace:

```shell
source devel/setup.bash
```

Run the launch file for the given assignment, e.g.:

```shell
roslaunch omtp_support visualize_omtp_factory.launch
```

## Lecture 2 - Git and Version Control

### Overview

The lecture was concerned with creating a git repository and setting up rules for committing.

<div style="page-break-after: always;"></div>

## Lecture 3 - Simulink Dynamic Motion Primitives Simulation

### Overview

This lecture did not contain any assignments, however, a Simulink control system was made for visualizing and learning how a Dynamic Motion Primitives functions, build in the same way as shown in the video lectures.

### Getting Started

#### Prerequisites

`MATLAB R2019b` with `Simulink (Version 10.0)` or newer version is required.

#### Running the Simulink simulation

First run the `init_force.m` MATLAB script to load the parameters into the workspace. In this script the gains `alpha_z` and `beta_z` can be modified accordingly to change the dampening of the system.

Secondly, run the `DMP.slx` Simulink file and click on any of the three scopes to show the plots for position, velocity and acceleration. Some parameters can be adjusted using the sliders or direct input that is: `Goal`, `tau`, `Fext`, and `Omega`.

<div style="page-break-after: always;"></div>

## Lecture 4 - Dynamic Motion Primitives in MuJoCo

## Overview

In this lecture the focus has been on implementing Dynamic Motion Primitives (DMP) in MATLAB and in the simulation environment MuJoCo. Some template files for MATLAB were provided together with the KUKA LWR manipulator for MuJoCo HAPTIX. In these template files the group has filled in the missing portions to make the program run as described by the objective for the assignment.

### Getting Started

#### Prerequisites

`Windows 10 64-bit` and `MATLAB R2019b` or newer version is required to use MuJoCo HAPTIX.

From the [MuJoCo download page](https://www.roboti.us/index.html) you will have to download the `mjhaptix150` and install it.

#### Download required files

Some additional files are necessary to run the simulation in MuJoCo and they can be downloaded from the [Course page on Moodle](https://www.moodle.aau.dk/course/view.php?id=32575&section=4), download `Exercises updated.zip`, extract the zip-folder in the directory you wish to run the simulation from. Now you are ready to clone the `lecture4\DMP_lib` folder into the previously made directory so that the files from this repository replaces the MATLAB script in the zip-folder.

In MATLAB you want to open the folder `DMP_lib` so that you can run the scripts, in addition the following folders: `apimex` and `Mujoco_lib` should be added to the path so that MATLAB can find the necessary functions to run the simulation.

### Assignment 1

In this assignment a DMP has been implemented in MATLAB. To run the DMP and see how it can follow a demonstrated trajectory(`test_trj.mat`) open the `DMP_lib\Test_CDMP.m` file and run the script.

The figures below shows how the DMP is able to follow the trajectory.

<p align="center">
<img src="L04_fig1.png" width="200"><img src="L04_fig2.png" width="200"><img src="L04_fig3.png" width="200">
</p>

### Assignment 2

In this assignment a DMP has been implemented in the `DMP_lib\MoveDMP_Mujoco.m` file, similar to the previous assignment the robot will follow a demonstrated trajectory(`test_trj.mat`). To run the simulation open the directory `.\program` and launch the `mjhaptix.exe` program. This will open MuJoCo HAPTIX and you will now have to load the simulation model into the program to run it. To do so, click on file menu-button(`ctrl+o`) and find the `LWR_table.xml` file in the `.\Haptix_models` directory.

You are now ready to start the simulation by pressing the play-button(`space-bar`). The simulation is now running and will be waiting for any MATLAB control input. Go back to `DMP_lib\MoveDMP_Mujoco.m` file and run the script so that the KUKA LWR manipulator will execute the trajectory.

<p align="center">
<img src="L04_assignment2.gif" width="300">
</p>

### Assignment 3

In this assignment the DMP similar to the one used in the previous assignment was adapted for the task of grasping cubes on the table. The objective was to use the DMP to plan a linear path between point-to-point movements to pick up the cubes and places them.

A `Test_move_grasp.m` script was provided showing how the robot picks and places the cubes using simple kinematics. The same approach was taken in `Test_dynamic_move_grasp.m`, however, two new functions were created to make use of DMP for joint moovement and cartesian movement. The same joint positions and world coordinates were used to give a target for where the robot should move. The `LearnJDMP.m`, `LearnQDMP.m`, `DMP_rlearn.m`, and `qDMP_learn.m` scripts were used as inspiration to create the functions `JmoveDynamic()` and `CmoveDynamic()` that would replace `JmoveM()` and `CmoveM()` respectively. To connect to the robot and move the joints the approach used in `MoveDMP_Mujoco.m` was followed. A comparison is seen below:

<p align="center">
<img src="L04_assignment3.gif" width="400">
</p>

*(The above GIF is at 2x speed)*

**TODO: Fix the funny behaviour of the arm and make sure it grasps correctly**

### Assignment 4

**TODO: Use the functionality of the previous assignment, but built a function that measures contact force of the manipulator, so that it detects collisions.**

<div style="page-break-after: always;"></div>

## Lecture 5 - Configuring MoveIt and using it in Gazebo

### Overview

The focus of this lecture was to become acquainted with the MoveIt setup assistant and how to use the package created from it to control the robots in the simulated factory environment.

### Getting started

### Assignment 1

The objective of this assignment was to configure the MoveIt package that controls the two UR5 robots using the setup assistant.

Source the workspace:

```shell
source devel/setup.bash
```

Launch:

```shell
roslaunch moveit_setup_assistant setup_assistant.launch
```

Generate a MoveIt configuration as follows:

- **Start** > Load the `omtp_factory.xacro` from the `omtp_support` package.
- **Self-Collisions** > Set sampling density to high and generate collision matrix
- **Virtual Joints** > Create a virtual joint `fixed_base1` with `world` as child link and `robot1_base_link` as parent
- **Virtual Joints** > Do the same thing for `robot2`
- **Planning Groups** > Create new planning group called `robot1` with *TRAC-IK Kinematics Solver* and *RRTConnect* planner
- **Planning Groups** > Add a kinematic chain with `robot1_base_link` as base and `vacuum_gripper1_suction_cup` as link
- **Planning Groups** > Do the same for `robot2`
- **Robot Poses** > Add Up and Home poses for both robots
- **ROS Control** > Use the *Auto Add FollowJointsTrajectory* Controller
- **Simulation** > Generate URDF
- **Configuration Files** > Set the Configuration path to ``omtp_course_ws/src/omtp/omtp_moveit_config``

### Using MoveIt

Build and source the workspace first:

```shell
catkin build
source devel/setup.bash
```

#### RViz

Launch the demo in Rviz:

```shell
roslaunch omtp_moveit_config demo.launch
```

### Assignment 2

The objective of the assignment is to execute a script of motions using MoveIt Commander.

#### MoveIt Commander

Launch MoveIt commander:

```shell
rosrun moveit_commander moveit_commander_cmdline.py
```

Test the commander with the following commands:

```shell
use robot1
go R1Up
go R1Home
go up 0.1
go down 0.3
help
```

To load commands from a script (eg. moveit_commander_test), use:

```shell
load moveit_commander_test
```

### Assignment 3

The objective is to use the move group Python interface to perform a pick and place task in the factory environment.

#### Move Group Python Interface

Make scripts executable:

```shell
chmod +x omtp_lecture5/scripts/*
```

Test the pick and place pipeline:

```shell
roslaunch omtp_lecture5 omtp_simple_pick_place.launch
```

Test assignment 3 script:

```shell
roslaunch omtp_lecture5 lecture5_assignment3.launch
```

#### Gazebo

Test our pick and place pipeline:

```shell
roslaunch omtp_lecture5 omtp_lecture5_environment.launch
roslaunch omtp_lecture5 omtp_lecture5_environment.launch gui:=false # no GUI
```

Note that Gazebo is launched in pause state to make sure that all controllers have been loaded. You can either press play in the GUI or from the terminal as follows:

```shell
rosservice call /gazebo/unpause_physics "{}"
```

### Final result

<p align="center">
<img src="L05_assignment3.gif" width="600">
</p>

<div style="page-break-after: always;"></div>

## Lecture 6 - Adding Logical Cameras to Gazebo

### Overview

In this lecture the topic is focused on object detection and grasping. In the Gazebo simulation two logical cameras are introduced and vacuum grippers are equipped on the two robots. The combination of the two will make it possible to manipulate objects in a dynamic environment by estimating the pose of the object of interest and manipulating it.

### Assignment 1

The logical camera 1 and 2 have been added to the simulation and can be viewed using the following:

Source the workspace:

```shell
source devel/setup.bash
```

Launch:

```shell
roslaunch omtp_lecture6 lecture6_assignment1.launch
```

### Assignment 2

The factory simulation is launched from the previous assignment and in a separate terminal `rosrun tf view_frames` is executed to view the transformation tree of all the objects within the simulation.

The tf tree can be viewed in the image below:

<p align="center">
<img src="L06_tf_tree.png" width="600">
</p>

### Assignment 3

In this assignment the objective was to create a pose transformation of the object to the frame of the vacuum suction gripper, so that the inverse kinematics can be applied to move the end-effector to the correct position.

Source the workspace and then launch:

```shell
roslaunch omtp_lecture6 lecture6_assignment3.launch
```

To spawn the object next to robot 2 use the following ROS-service:

```shell
rosservice call /spawn_object_at_robot2
```

A box object is added to the factory floor and the `lecture6_assignment3.py` script is edited to implement a subscriber that listens to the `logical_camera_2` topic, here the relevant reference frame for the pose of the object can be obtained.

The frame of the object is then transform to the `vacuum_gripper2_suction_cup` reference frame so that the movement from pre-grasp position and to the object can be calculated.

### Assignment 4

In this assignment a `lecture6_assignment4.py` script was written to move to the location of the object, the gripper is activated to suck the object, move to the location above the bin and release the object by disabling the gripper.

<p align="center">
<img src="L06_assignment4.gif" width="600">
</p>

### Assignment Challenge

**TODO: The challenge was to fill the bin with objects**

<div style="page-break-after: always;"></div>

## Lecture 7 - Implementing a FlexBE Finite State Machine in Gazebo

### Overview

In this lecture FlexBE, the flexible behavior engine, is used to create a finite state machine (FSM) for the manipulation of objects on the conveyor belt in the OMTP factory. The assignment demonstrates a pick and place behavior where the conveyor moves the object to the camera position and the FSM will move the manipulator in position to grasp the object and move it.

### Assignment 1

In the **FlexBE App** a finite state machine has been created to use the camera to detect the object on the conveyor belt. When the pose of the object is retrieved a pre-grasp pose of the robot is calculated and the manipulator is moved right above the object. Next, the grasp pose is calculated and the robot is moved to grasp the object by activating the vacuum gripper. Then the object is moved by the manipulator to the home position.

Source the workspace:

```shell
source devel/setup.bash
```

Launch:

```shell
roslaunch omtp_gazebo omtp_pick_demo.launch
roslaunch flexbe_app flexbe_full.launch
```

Spawn the object on conveyor:

```shell
rosservice call /start_spawn
```

Note: the position of the spawned object was modified in `omtp_gazebo/config/conveyor_object.yaml` such that the object initially appears outside of the camera's view, and is then brought to the robot using the conveyor belt.

In the **FlexBE App** window load the `Robot1 pick up from conveyor` state machine from the `omtp_factory_flexbe_behaviors` package and execute the behavior in the Runtime Control window.

The FSM made in **FlexBE**:

<p align="center">
<img src="L07_assignment1.png" width="400">
</p>

Video of the movement executed in **Gazebo**:

<p align="center">
<img src="L07_assignment1.gif" width="400">
</p>

*(The above GIF is at 2x speed)*

<div style="page-break-after: always;"></div>

## Lecture 8 - Implementing CNNs and DL in Google Colab

### Overview

In this lecture the the focus was on Convolutional Neural Networks (CNN) and Deep Learning (DL). A practical example was given and assignments were carried out in Google Colab.

### Getting Started

#### Prerequisites

Any internet browser and a Google account with available space on Drive will work. However, for some assignments `Google Chrome` browser and a **camera** is required.

#### Download required files

Some additional files are necessary to run the Google Colab environment and they can be downloaded from the [Course page on Moodle](https://www.moodle.aau.dk/course/view.php?id=32575&section=8), in the `Lecture material` folder download `covid19_colab.zip`, extract the zip-folder anywhere to your computer and upload it to your Google Drive. From here you want to open the `covid19_mask.ipynb` Notebook document.

### Assignment 1

In this assignment the code was executed in Google Colab. All group members recorded a video of themselves without a facemask and then equipped one during the video to show that the detection of a facemask worked.

<p align="center">
<img src="L08_gala.gif" width="200"><img src="L08_gui.gif" width="200"><img src="L08_jacob.gif" width="200">
</p>

<p align="center">
<img src="L08_jan.gif" width="200"><img src="L08_lars.gif" width="200"><img src="L08_rune.gif" width="200">
</p>

### Optional Assignment 1

**TODO: Exchange the MobileNetv2 with a network of our choice.**

### Optional Assignment 2

**TODO: Use the detection algorithm in the OMTP environment to train the simulated camera to identify two classes of any kind of objects.**

<div style="page-break-after: always;"></div>

## Lecture 9 - Implementing Deep Reinforcement Learning with OpenAI Gym

### Overview

The focus of this lecture was deep reinforcement learning for robotics applications. A simple demo running DQN, PPO1, PPO2, and TRPO on the CartPole envrionment from OpenAI Gym.

### Getting Started

#### Prerequisites

Make sure to have pip version >=19

```shell
pip install --upgrade pip
```

**Recommended:** Install and create a virtual python environment.

```shell
pip3 install virtualenv
```

In the `/lecture9/` folder create the virtual environment named `venv` using:

```shell
virtualenv venv
```

Before using the virtual environment source the workspace:

```shell
source ./venv/bin/activate
```

Now you are ready to install the dependencies:

```shell
pip3 install tensorflow==1.15.0
pip3 install tensorflow-gpu==1.15.0
pip install gym
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev
pip install stable-baselines
```

Additional dependencies that may be necessary (your milage may vary).

```shell
pip install --upgrade setuptools
pip install stable-baselines[mpi]
```

### Assignment 1

To train the model and run the animation of the inverted pendulum in the terminal window that sourced the virtual environment run the following commmand:

```shell
python3 cart_pole_DQN.py
```

Where you can replace `DQN` with any of `PPO1`, `PPO2`, `TRPO` to train a different deep reinforcement learning algorithm.

To view the graphs of the different models with tensorboard use the following command and open the website `http://localhost:6006/`

```shell
tensorboard --logdir logs
```

The models we trained can be seen in the plot below:

<p align="center">
<img src="L09_Graph.png" width="600">
</p>

In the following animation the behaviour of the CartPole running a DQN algorithm can be seen:

<p align="center">
<img src="L09_pole-cart-DQN.gif" width="400">
</p>

## Acknowledgements

* Aalborg University
* Dimi and Simon
